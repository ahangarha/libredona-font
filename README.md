[English](./README-en.md)

# قلم LibreDona

قلم LibreDona،
یک قلم آزاد است که برای
[دورهمی نرم‌افزار آزاد (دونا)](https://do-na.ir)
ساخته شده است.
این قلم، اعدادی را ارائه می‌دهد که با شکل لوگوی این رویداد تناسب دارند.


![LibreDona-digit demo](misc/graphics/promo.png)

## دریافت

قالب مورد نظرتان را می‌توانید از
[پوشهٔ dist](https://framagit.org/ahangarha/libredona-font/-/tree/main/dist)
دریافت کنید.

## پروانهٔ نشر

این قلم تحت پروانهٔ OFL منتشر شده است.
