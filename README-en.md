# LibreDona Font

LibreDona is a Free/OpenSource font made for
[Dona meetup](https://do-na.ir)
in Iran.
This font provides digits matching the Dona logo design.

![LibreDona-digit demo](misc/graphics/promo.png)

## Download

Get your desired font format from
[dist directory](https://framagit.org/ahangarha/libredona-font/-/tree/main/dist).

## License

This font is under OFL.
